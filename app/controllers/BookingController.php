<?php

class BookingController extends ControllerBase
{

    public function indexAction()
    {

    }

    public function listAction()
    {
        $response = HttpResponseManager::getResponseInstance();

        if ($all_bookings = Gostovanje::find()) {
            $all_bookings_array = array();
            foreach ($all_bookings as $booking) {
                if ($booking->datum_dolaska === null) {
                    $booking->datum_dolaska = '-';
                }

                if ($booking->datum_odlaska === null) {
                    $booking->datum_odlaska = '-';
                }

                $all_bookings_array[] = array(
                    'id' => $booking->gostovanje_id,
                    'fname' => $booking->Osoba->ime,
                    'lname' => $booking->Osoba->prezime,
                    'email' => $booking->Osoba->email,
                    'pnumber' => $booking->Osoba->kontakt_broj,
                    'country' => $booking->Parcela->Kamp->Grad->Drzava->naziv,
                    'city' => $booking->Parcela->Kamp->Grad->grad_ime,
                    'campName' => $booking->Parcela->Kamp->kamp_ime,
                    'campID' => $booking->Parcela->Kamp->kamp_id,
                    'parcelaCode' => $booking->Parcela->sifra_parcele,
                    'parcelaId' => $booking->Parcela->parcela_id,
                    'ardate' => $booking->datum_dolaska,
                    'dpdate' => $booking->datum_odlaska,
                );
            }

            $response->setStatusCode(200, "OK");
            $content = new DataType();
            $content->setStrategy(new JSONStrategy());
            $content->get_coded_data($all_bookings_array);
        } else {
            $response->setStatusCode(404, "Not Found");
            $response->setContentType('text/plain', 'UTF-8');
            $response->setContent("No booking records found in the database!");
        }

        $response->send();
        //$this->view->tmpDat = $response->getContent();
        //$this->view->tmpDat = $all_bookings_array;
    }


    public function registrationAction()
    {
        $response = HttpResponseManager::getResponseInstance();
        $json_content = $this->request->getJsonRawBody();

        if ($osoba = Osoba::findFirstByEmail($json_content->email)) {
            if (isset($json_content->gostovanjeId)) {
                if ($gostovanje = Gostovanje::findFirstByGostovanjeId($json_content->gostovanjeId)) {
                    // Delete existing booking
                    if ($this->request->isDelete()) {
                        if ($gostovanje->delete()) {
                            $dostupnost_parcele = Dostupnost::findFirst(
                                array(
                                    'conditions' => 'parcela_id = ?1 AND datum_od = ?2 AND datum_do = ?3',
                                    'bind' => array(
                                        1 => $gostovanje->parcela_id,
                                        2 => $gostovanje->datum_dolaska,
                                        3 => $gostovanje->datum_odlaska
                                    )
                                )
                            );

                            if ($dostupnost_parcele->delete()) {
                                $message_for_deletion = "DostupnostID: $dostupnost_parcele->dostupnost_id deleted!";
                            } else {
                                $message_for_deletion = "Error: Couldn't delete dostupnost!";
                            }

                            $response->setStatusCode(200, "OK");
                            $response->setContentType('text/plain', 'UTF-8');
                            $response->setContent("Booking (ID: $gostovanje->gostovanje_id) successfully deleted! + $message_for_deletion");
                        } else {
                            $response->setStatusCode(204, "No Content");
                            $response->setContentType('text/plain', 'UTF-8');
                            $response->setContent("Error: Failed to delete booking (ID: $gostovanje->gostovanje_id)!");
                        }
                        $response->send();
                        return;
                    }

                    // Update existing booking
                    if ($this->request->isPut()) {
                        if ($json_content->ardate >= $json_content->dpdate) {
                            $response->setStatusCode(409, "Conflict");
                            $response->setContentType('text/plain', 'UTF-8');
                            $response->setContent("Error: Arival date cannot be after departure date (min. 1 day booking)!");
                            $response->send();
                            return;
                        }

                        $gostovanje->datum_rezervacije = date('Y-m-d');

                        $message_for_placanje = "";
                        // User left?
                        if ($json_content->userLeft === "da") {
                            if (($placanje = PlacanjeController::placanje_check($gostovanje->gostovanje_id)) == 1) {
                                $message_for_placanje .= " + placanje succesfully added!";
                            } else if ($placanje == 2) {
                                $message_for_placanje .= " + Error: Placanje was not added!";
                            }
                        }

                        // Is parcela available on desired date?
                        if ($is_parcela_avail = Dostupnost::find(
                            array(
                                'conditions' => 'parcela_id = ?1 AND datum_od <= ?2 AND datum_do >= ?3',
                                'bind' => array(
                                    1 => $json_content->parcelaId,
                                    2 => $json_content->dpdate,
                                    3 => $json_content->ardate
                                ),
                                'for_update' => true
                            )
                        )->toArray()) {
                            $response->setStatusCode(409, "Conflict");
                            $response->setContentType('text/plain', 'UTF-8');
                            $response->setContent("Error: Parcela($json_content->parcelaId) unavailable for requested time period! $message_for_placanje");

                            $response->send();
                            return;
                        } else {
                            $old_dostupnost = Dostupnost::findFirst(
                                array(
                                    'conditions' => 'parcela_id = ?1 AND datum_od = ?2 AND datum_do = ?3',
                                    'bind' => array(
                                        1 => $gostovanje->parcela_id,
                                        2 => $gostovanje->datum_dolaska,
                                        3 => $gostovanje->datum_odlaska
                                    )
                                )
                            );

                            // Update availability
                            $old_dostupnost->datum_od = $json_content->ardate;
                            $old_dostupnost->datum_do = $json_content->dpdate;
                            if ($old_dostupnost->update()) {
                                $message_for_deletion = "Dostunost id: $old_dostupnost->dostupnost_id successfully updated!";
                            } else {
                                $message_for_deletion = "Error: Couldn't update dostupnost!";
                            }

                            // Update booking
                            $gostovanje->datum_dolaska = $json_content->ardate;
                            $gostovanje->datum_odlaska = $json_content->dpdate;

                            // Update in osoba
                            $osoba->ime = $json_content->fname;
                            $osoba->prezime = $json_content->lname;
                            $osoba->kontakt_broj = $json_content->pnumber;

                            if ($gostovanje->update() && $osoba->update()) {
                                $response->setStatusCode(200, "OK");
                                $response->setContentType('text/plain', 'UTF-8');
                                $response->setContent("Booking (ID: $gostovanje->gostovanje_id) successfully updated + $message_for_deletion + $message_for_placanje");
                            } else {
                                $response->setStatusCode(204, "No Content");
                                $response->setContentType('text/plain', 'UTF-8');
                                $response->setContent("Error: Failed to delete booking (ID: $gostovanje->gostovanje_id)!");
                            }

                            $response->send();
                            return;
                        }
                    }
                }
            }

            // Add new booking
            if ($this->request->isPost()) {
                if ($json_content->ardate >= $json_content->dpdate) {
                    $response->setStatusCode(409, "Conflict");
                    $response->setContentType('text/plain', 'UTF-8');
                    $response->setContent("Error: Arival date cannot be after departure date (min. 1 day booking)!");
                    $response->send();
                    return;
                }

                if (!$is_parcela_avail = Dostupnost::find(
                    array(
                        'conditions' => 'parcela_id = ?1 AND datum_od <= ?2 AND datum_do >= ?3',
                        'bind' => array(
                            1 => $json_content->parcelaId,
                            2 => $json_content->dpdate,
                            3 => $json_content->ardate
                        ),
                        'for_update' => true
                    )
                )->toArray()) {
                    $gostovanje = new Gostovanje();
                    $dostupnost = new Dostupnost();

                    $gostovanje->osoba_id = $osoba->osoba_id;
                    $gostovanje->datum_rezervacije = date('Y-m-d');
                    $gostovanje->datum_dolaska = $json_content->ardate;
                    $gostovanje->datum_odlaska = $json_content->dpdate;
                    $gostovanje->parcela_id = $json_content->parcelaId;

                    $dostupnost->parcela_id = $json_content->parcelaId;
                    $kamp = Parcela::findFirstByParcelaId($json_content->parcelaId);
                    $dostupnost->kamp_id = $kamp->kamp_id;
                    $dostupnost->dostupnost_status = $json_content->availability;
                    $dostupnost->opis = $json_content->desc;
                    $dostupnost->datum_od = $gostovanje->datum_dolaska;
                    $dostupnost->datum_do = $gostovanje->datum_odlaska;

                    if ($gostovanje->save() && $dostupnost->save()) {
                        $response->setStatusCode(201, "Created");
                        $response->setContentType('text/plain', 'UTF-8');
                        $response->setContent("Booking and availability successfully added!");
                    } else {
                        $response->setStatusCode(500, "Internal Server Error");
                        $response->setContentType('text/plain', 'UTF-8');
                        $response->setContent("Error: Booking and availability was not added!");
                    }
                } else {
                    $response->setStatusCode(409, "Conflict");
                    $response->setContentType('text/plain', 'UTF-8');
                    $response->setContent("Error: Parcela($json_content->parcelaId) unavailable for requested time period!");
                }
                $response->send();
                return;
            }
        }
    }
}

