<?php

class PlacanjeController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {
        $response = HttpResponseManager::getResponseInstance();

        $all_placanja_data = array();
        if ($placanja = Placanje::find()) {
            $test_placanja = $placanja->toArray();
            if (!empty($test_placanja)) {
                foreach ($placanja as $one_placanje) {
                    $all_placanja_data[] = array(
                        'fname' => $one_placanje->Gostovanje->Osoba->ime,
                        'lname' => $one_placanje->Gostovanje->Osoba->prezime,
                        'country' => $one_placanje->Gostovanje->Osoba->Grad->Drzava->naziv,
                        'city' => $one_placanje->Gostovanje->Osoba->Grad->grad_ime,
                        'camp' => $one_placanje->Gostovanje->Parcela->Kamp->kamp_ime,
                        'ardate' => $one_placanje->Gostovanje->datum_dolaska,
                        'dpdate' => $one_placanje->Gostovanje->datum_odlaska,
                        'nonights' => $one_placanje->nocenja,
                        'ppnight' => $one_placanje->cijena_nocenja,
                        'tax' => $one_placanje->porez,
                        'ukupno' => $one_placanje->ukupno
                    );
                }
            }
        }
        $response->setStatusCode(200, "OK");
        $content = new DataType();
        $content->setStrategy(new JSONStrategy());
        $content->get_coded_data($all_placanja_data);

        $response->send();
    }

    public static function placanje_check($gostovanjeId){

        if ($gostovanje = Gostovanje::findFirstByGostovanjeId($gostovanjeId)) {
            if ($exist_placanje = Placanje::findFirstByGostovanjeId($gostovanje->gostovanje_id)) {
                $placanje = $exist_placanje;
            } else {
                $placanje = new Placanje();
            }

            // Calculate number of days staying
            $nocenja_od = new DateTime($gostovanje->datum_dolaska);
            $nocenja_do = new DateTime($gostovanje->datum_odlaska);

            $number_nocenja = $nocenja_do->diff($nocenja_od);

            $placanje->gostovanje_id = $gostovanje->gostovanje_id;
            $placanje->nocenja = ($number_nocenja->days) + 1;
            $placanje->cijena_nocenja = ($placanje->nocenja * $gostovanje->Parcela->cijena_nocenja);
            $placanje->porez = 23.00;
            $placanje->ukupno = round((($placanje->cijena_nocenja * ($placanje->porez / 100)) + $placanje->cijena_nocenja), 2);

            if ($placanje->save()) {
                return 1;
            } else {
                return 2;
            }
        } else {
            return 3;
        }
    }
}

