<?php

class SessionController extends ControllerBase
{

    public function indexAction()
    {
        $auth = $this->session->get('auth');
        if ($auth) {
            $this->dispatcher->forward(
                array(
                    'controller' => 'session',
                    'action'     => 'work'
                )
            );
            return;
        }
    }

    private function _registerSession($user)
    {
        $this->session->set(
            'auth',
            array(
                'id'   => $user->osoba_id,
                'mail' => $user->email,
                'tip'  => $user->TipOsobe->tip
            )
        );
    }

    public function startAction()
    {
        if ($this->request->isPost()) {
            $email    = $this->request->getPost('email');
            $password = $this->request->getPost('password');


            if ($user = Osoba::findFirst(
                array(
                    'conditions' => "email = ?1 AND password = ?2",
                    'bind' => array(
                        1 => $email,
                        2 => sha1($password)
                    )
                )
            )) {
                $this->_registerSession($user);

                $this->dispatcher->forward(
                    array(
                        'controller' => 'session',
                        'action'     => 'work'
                    )
                );
                return;
            }
        }

        // Forward to the login form again
        $this->dispatcher->forward(
            array(
                'controller' => 'session',
                'action'     => 'index'
            )
        );
    }

    public function workAction()
    {
        // Display loggout button
        // Write something like... "You are now in session!"
        // Test other controllers to make sure they are working
        //$this->view->authData = $this->session->get('auth');

        $this->view->setVar(
            "authData",
            $this->session->get('auth')
        );
    }

    public function endAction()
    {
        $this->session->remove('auth');
        $this->dispatcher->forward(
            array(
                'controller' => 'index',
                'action'     => 'index'
            )
        );
    }

}

