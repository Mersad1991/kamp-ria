<?php
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl;
use Phalcon\Acl\Role;
use Phalcon\Acl\Adapter\Memory as AclList;
use Phalcon\Acl\Resource;

class SecurityPlugin extends Plugin
{
    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
        // Check whether the "auth" variable exists in session to define the active role
        $auth = $this->session->get('auth');
        if (!$auth) {
            $role = 'Gost';
        } else {
            $role = $auth['tip'];
        }

        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();

        $acl = $this->getAcl();

        $allowed = $acl->isAllowed($role, $controller, $action);

        if ($allowed != Acl::ALLOW) {
            // If he doesn't have access forward him to the index controller
            $dispatcher->forward(
                array(
                    'controller' => 'index',
                    'action'     => 'index'
                )
            );

            // Returning "false" we tell to the dispatcher to stop the current operation
            return false;
        }
    }

    public function getAcl()
    {
        if (!isset($this->persistent->acl)) {
            $acl = new AclList();

            $acl->setDefaultAction(Acl::DENY);

            // Register roles
            $roles = [
                'korisnik'  => new Role(
                    'Korisnik',
                    'Member privileges, granted after sign in.'
                ),
                'gost' => new Role(
                    'Gost',
                    'Anyone browsing the site who is not signed in is considered to be a "Guest".'
                ),
                'administrator' => new Role(
                    'Administrator',
                    'Admin privileges, granted after sign in.'
                )
            ];

            foreach ($roles as $role) {
                $acl->addRole($role);
            }

            //Private area resources - Admin
            $privateResourcesAdmin = array(
                'drzava'     => array('add'),
                'grad'       => array('add'),
                'kamp'       => array('index', 'add'),
                'parcela'    => array('picture', 'add'),
                'placanje'   => array('index')
            );
            foreach ($privateResourcesAdmin as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }

            //Private area resources - Korisnik
            $privateResources = array(
                'booking'    => array('index', 'list', 'registration'),
                'grad'       => array('list'),
                'session'    => array('work'),
                'placanje'   => array('index')
            );
            foreach ($privateResources as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }

            //Public area resources - Gost
            $publicResources = array(
                'index'      => array('index'),
                'drzava'     => array('index'),
                'grad'       => array('index', 'list'),
                'parcela'    => array('index', 'availability', 'list'),
                'search'     => array('index', 'camps'),
                'session'    => array('index', 'register', 'start', 'end'),
                'user'       => array('index', 'register')
            );
            foreach ($publicResources as $resource => $actions) {
                $acl->addResource(new Resource($resource), $actions);
            }

            //Grant access to public areas to both users and guests
            foreach ($roles as $role) {
                foreach ($publicResources as $resource => $actions) {
                    foreach ($actions as $action){
                        $acl->allow($role->getName(), $resource, $action);
                    }
                }
            }

            //Grant access to private area to role Korisnik and Admin
            foreach ($roles as $role) {
                if ($role->getName() === 'Administrator') {
                    foreach ($privateResourcesAdmin as $resourceAdmin => $actionsAdmin) {
                        foreach ($actionsAdmin as $actionAdmin) {
                            $acl->allow($role->getName(), $resourceAdmin, $actionAdmin);
                        }
                    }
                }

                if ($role->getName() !== 'Gost') {
                    foreach ($privateResources as $resource => $actions) {
                        foreach ($actions as $action) {
                            $acl->allow($role->getName(), $resource, $action);
                        }
                    }
                }
            }

            //The acl is stored in session, APC would be useful here too
            $this->persistent->acl = $acl;
        }

        return $this->persistent->acl;
    }
}